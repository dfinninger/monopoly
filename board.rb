# this file holds the board

require '/./tile'

class Board

  def initialize
    @tiles = []
    counter = 0
    contents = File.read('tile_contents.txt')
    contents.each do |line|
      reg_ex = /^
					(<?name>[^|])|				    # looks for the tile name
					(<?costs>[^|])|				    # tile cost array (rent)
					(<?purchase_price>[^|])|	# initial purchase price
					(<?group>[^|])|				    # group (color)
					(<?property>[^|])			    # property? (boolean)
				$/x
      data = reg_ex.match(line)
      @tiles += Tile.new(data[:name],
                         data[:costs],
                         data[:purchase_price],
                         data[:group],
                         data[:property])
      counter += 1
      #  TODO: add in "next tile" functionality
      #+ that gives the name of the current tile
      #+ back to the previous one.
    end
  end


end