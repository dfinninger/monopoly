# this is the player file

class Player

	attr_accessor 	:cash, :properties
	attr_reader		:name

	def initialize(	name		= nil,
					cash 		= 0,
					properties 	= [])
		@name 		= name
		@cash 		= cash
		@properties = properties
	end

end