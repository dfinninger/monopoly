# This is the tile class. Defines each tile on the board.

class Tile

  attr_reader :name, :purchase_price, :group, :property
  attr_accessor :houses, :hotels, :owner, :mortgaged, :next_tile

  def initialize(name = 'No Name',
      costs = [],
      purchase_price = 0,
      group = nil,
      property = true,
      houses = 0,
      hotels = 0,
      owner = 0,
      mortgaged = false,
      next_tile = nil)
    @name = name
    @costs = costs
    @purchase_price = purchase_price
    @houses = houses
    @group = group
    @property = property
    @hotels = hotels
    @owner = owner
    @mortgaged = mortgaged
    @next_tile = next_tile
  end

  def rent # this adjusts the cost of landing on the property
    buildings = @houses + (@hotels * 5)
    @costs[buildings]
  end

end